from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import generic
from django.db.models import Q
from django.views.generic import ListView,CreateView,UpdateView,DeleteView,DetailView
from apps.principal.forms import DireccionFormulario ,EquipoFormulario,RelacionFormulario,PerfilFormulario,RelacionPerfilFormulario,RedFormulario
from apps.principal.forms import InfraestructuraFormulario,SecretariaFormulario,UnidadFormulario,AreaFormulario
from apps.principal.forms import InfraestructuraSecretariaFormulario,InfraestructuraUnidadFormulario,InfraestructuraAreaFormulario

from apps.principal.models import Direccion,Equipo,Relacion,Perfil,RelacionPerfil,Red,Infraestructura,Secretariauno,Unidaduno,Areauno

from apps.principal.models import InfraestructuraSecretaria,InfraestructuraUnidad,InfraestructuraArea

from apps.principal.models import SecretariaArea,SecretariaUnidad
from apps.principal.forms import SecretariaAreaFormulario,SecretariaUnidadFormulario

from apps.principal.models import PefilSecretaria,PefilArea,PefilUnidad
from apps.principal.forms import PefilSecretariaFormulario,PefilUnidadFormulario,PefilAreaFormulario

from apps.principal.models import Secretaria,Unidad,Area
from apps.principal.forms import SecretariavFormulario,AreavFormulario,UnidadvFormulario

from apps.principal.models import RedSecretaria,RedArea,RedUnidad
from apps.principal.forms import RedSecretariaFormulario,RedUnidadFormulario,RedAreaFormulario



from django.shortcuts import render_to_response
from django.shortcuts import render,get_object_or_404
# Create your views here.



## ..........................................:_::::::::::::.......Direccion..........::::::::::........................................

def index(request):
    return render(request,'principal/index.html')

def direccion_view(request):
    if request.method == 'POST':
        form = DireccionFormulario(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('direccion:index')
       # return redirect('principal:index')
            return redirect(index)
    else:
        form = DireccionFormulario()
        
    return render(request,'principal/direccion/direccion_formulario.html',{'form_direccion':form})

def direccion_list(request):
    direccion= Direccion.objects.all().order_by('id')
    contexto={'direccionlist':direccion}
    return render(request,'principal/direccion/direccion_list.html',contexto)

#editar con funciones 
def direccion_edit(request,id_direccion):
    direccion=Direccion.objects.get(id=id_direccion)
    if request.method == 'GET':
        form =DireccionFormulario(instance=direccion)
    else:
        form=DireccionFormulario(request.POST,instance=direccion)
        if form.is_valid():
            form.save()
        return redirect(direccion_list)
    return render(request,'principal/direccion/direccion_formulario.html',{'form_direccion':form})

def direccion_delete(request,id_direccion):
    direccion=Direccion.objects.get(id=id_direccion)
    if request.method =='POST':
        direccion.delete()
        return redirect(direccion_list)
    return render(request,'principal/direccion/direccion_delete.html',{'direccion':direccion})

#................................................vistas basadas en clases DIRECCION ....................................................
#Lista de Direcciones
class DireccionList(ListView):
    model =Direccion
    template_name ='principal/direccion/direccion_list.html'
  
    def get_queryset(self):
       return Direccion.objects.order_by('id')
    
    #def get_context_data(self,*args, **kwargs):
      
       # context=super().get_context_data(*args,**kwargs)
       # context['direccion_list']=self.get_object().relacion_set.all() 
       # return context



class DireccionCreate(CreateView):
    model =Direccion
    form_class=DireccionFormulario
    template_name='principal/direccion/direccion_formulario.html'
    success_url=reverse_lazy('direccion_listar')
    
class DirecionUpdate(UpdateView):
    model =Direccion
    form_class=DireccionFormulario
    template_name='principal/direccion/direccion_formulario.html'
    success_url=reverse_lazy('direccion_listar')
    
class DireccionDelete(DeleteView):
    model =Direccion
    form_class=DireccionFormulario
    template_name='principal/direccion/direccion_delete.html'
    success_url=reverse_lazy('direccion_listar')


class DireccionDetail(DetailView):
    model =Direccion
    template_name ='principal/direccion/direccion_list_detail.html'
 
    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['direccion_list']=self.get_object().relacion_set.all() #aca llamoa al modelo relacionperfil como objeto tambienpuedo hacerlo con otros modelos
        return context



#...........................................:::.....vistas basadas en clases Equipo ......::::..............................................


class EquipoList(ListView):
    model =Equipo
    template_name ='principal/equipo/equipo_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return Equipo.objects.order_by('id')

class EquipoCreate(CreateView):
    model =Equipo
    form_class=EquipoFormulario
    template_name='principal/equipo/equipo_formulario.html'
    success_url=reverse_lazy('equipo_listar')
    
class EquipoUpdate(UpdateView):
    model =Equipo
    form_class=EquipoFormulario
    template_name='principal/equipo/equipo_formulario.html'
    success_url=reverse_lazy('equipo_listar')
    
class EquipoDelete(DeleteView):
    model =Equipo
    form_class=EquipoFormulario
    template_name='principal/equipo/equipo_delete.html'
    success_url=reverse_lazy('equipo_listar')


class EquipoDetail(DetailView):
    model =Equipo
    template_name='principal/equipo/equipo_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(EquipoDetail, self).get_context_data(**kwargs)
   
        return context
class EquipoDetailv(DetailView):
    model =Perfil
    template_name='principal/equipo/equipo_detailv_list.html'

    def get_context_data(self, **kwargs):
        context = super(EquipoDetailv, self).get_context_data(**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context
     
    
        

#...........................................:::.....vistas basadas en clases Relacion ......::::..............................................


class RelacionList(ListView):
    model =Relacion
    template_name ='principal/relacion/relacion_list.html'
  
    def get_queryset(self):
        return Relacion.objects.order_by('id')

class RelacionCreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/relacion_formulario.html'
    success_url=reverse_lazy('relacion_listar')


    
class RelacionUpdate(UpdateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/relacion_formulario.html'
    success_url=reverse_lazy('relacion_listar')
    
class RelacionDelete(DeleteView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/relacion_delete.html'
    success_url=reverse_lazy('relacion_listar')


class IP_RelacionCreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/ip_relacion_formulario.html'
    success_url=reverse_lazy('red_listar')
    def get_initial(self):

        direccion=Direccion.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "direccion":direccion,
            "esta_vigente":esta_vigente

        }

class Equipo_ipRelacionCreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/equipo_ip_relacion_formulario.html'
    success_url=reverse_lazy('perfil_listar')
    def get_initial(self):
        equipo=Equipo.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "equipo":equipo,
            "esta_vigente":esta_vigente
        }

class Equipo_ipRelacionCreateDos(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/equipo_ip_relacion_formulariodos.html'
    success_url=reverse_lazy('perfil_listar')
    def get_initial(self):
        equipo=Equipo.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "equipo":equipo,
            "esta_vigente":esta_vigente
        }


   


#...........................................:::.....vistas basadas en clases Perfil ......::::..............................................


class PerfilList(ListView):
    model =Perfil
    template_name ='principal/perfil/perfil_list.html'

    def get_queryset(self):
        return Perfil.objects.order_by('id')

class PerfilListall(ListView):
    model =Perfil
    template_name ='principal/perfil/perfil_list_all.html'
    
    def get_queryset(self):
        return Perfil.objects.order_by('id')

class PerfilCreate(CreateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_formulario.html'
    queryset=Perfil.objects.all()
    def get_success_url(self):
        return reverse_lazy('perfil_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
        

    
class PerfilUpdate(UpdateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_edit.html'
    
    success_url=reverse_lazy('secretariav_listar')

class PerfilUpdateD(UpdateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_editD.html'
    
    success_url=reverse_lazy('perfil_listar_all')

class PerfilUpdateT(UpdateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_editT.html'
    
    success_url=reverse_lazy('secretariav_listar')
    

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        
        return context
    
class PerfilDelete(DeleteView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_delete.html'
    success_url=reverse_lazy('secretariav_listar')

class PerfilDeleteD(DeleteView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_deleteD.html'
    success_url=reverse_lazy('perfil_listar_all')


class PerfilDetail(DetailView):
    model =Perfil
    template_name='principal/perfil/perfil_detail_list.html'

    def get_context_data(self, **kwargs):
        context=super(PerfilDetail,self).get_context_data(**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context

class PerfilDetailD(DetailView):
    model =Perfil
    template_name='principal/perfil/perfil_detail_list.html'

    def get_context_data(self, **kwargs):
        context=super(PerfilDetail,self).get_context_data(**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context


class PerfilDetailView(DetailView):
    model =Perfil
    template_name='principal/perfilEI/perfilEI_detail_list_view.html'

    def get_context_data(self, **kwargs):
        context = super(PerfilDetailView, self).get_context_data(**kwargs)
        context['equipo_list'] = Equipo.objects.filter(perfil1=self.object)
        return context

    
    
   
#...........................................:::.....vistas basadas en clases Relacion Perfil ......::::..............................................


class RelacionPerfilList(ListView):
    model =RelacionPerfil
    template_name ='principal/relacionPerfil/relacionPerfil_list.html'

    def get_queryset(self):
        return RelacionPerfil.objects.order_by('id')

class RelacionPerfilCreate(CreateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_formulario.html'
    success_url=reverse_lazy('perfil_listar')
    
    def get_initial(self):

        perfil=Perfil.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "perfil":perfil,
            "esta_vigente":esta_vigente
            
        }

class RelacionPerfilCreateDos(CreateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_formulariodos.html'
    success_url=reverse_lazy('perfil_listar')
    
    def get_initial(self):

        perfil=Perfil.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "perfil":perfil,
            "esta_vigente":esta_vigente
            
        }


    
class RelacionPerfilUpdate(UpdateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_edit.html'
    success_url=reverse_lazy('perfil_listar')
    
class RelacionPerfilDelete(DeleteView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_delete.html'
    success_url=reverse_lazy('perfil_listar')

#...........................................:::.....Vistas Genericas Perfil......::::..............................................


class PerfilEquipoDetail(DetailView):
    model =Perfil
    template_name ='principal/VistasUsuario/perfil_user_detail.html'
   
    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context


#############################################################################################################################
# Ver listas de equipos registrados por ususario INGENIERO
class PerfilDetailno(DetailView):
    model =Perfil
    template_name ='principal/VistasUsuario/perfil_detail.html'
    
    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() #aca llamoa al modelo relacionperfil como objeto tambienpuedo hacerlo con otros modelos
        return context
###########################################################################################################################@@


#:.........................................................:::::.Lista de usuarios y Equipos :::................................   
  
    
#Mostrar Todos los usuarios y los equipos
class PerfilDetailAll(ListView):
    model =RelacionPerfil
  
    template_name ='principal/VistasUsuario/perfil_user_detail_all.html'
  
    def get_queryset(self):
        return RelacionPerfil.objects.order_by('id')


#...........................................:::.....vistas basadas en clases Red ......::::..............................................




class REdList(ListView):
    model =Red
    template_name ='principal/red/red_list.html'
  
    print(ListView.get)

    def get_queryset(self):
        buscar =self.request.GET.get('buscar')
        redes= Red.objects.all()

        if buscar:
            redes=Red.objects.filter(nombre__icontains=buscar )| Red.objects.filter(puerta_enlace__contains=buscar)
            try:
                redes=redes|Red.objects.filter(numero=int(buscar))
            except:
                pass
        return redes.order_by('id')



class REDCreate(CreateView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_formulario.html'
    
    def get_success_url(self):
        self.object.generarIps()
        return reverse_lazy('red_listar')


class REDCreateDos(CreateView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_formulario2.html'
    
    def get_success_url(self):
        self.object.generarIps()
        return reverse_lazy('red_creardos')



class REDUpdate(UpdateView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_formulario.html'
    success_url=reverse_lazy('red_listar')
    
class REDDelete(DeleteView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_delete.html'
    success_url=reverse_lazy('red_listar')

class RedDetail(DetailView):
    model =Red
    template_name ='principal/red/red_detail.html'
    
    def get_context_data(self,*args, **kwargs):
        context=super().get_context_data(*args,**kwargs)
        context['red_list']=self.get_object().direccion_set.all()
        return context

    def get_queryset(self):
        buscar =self.request.GET.get('buscar')
        redes= Red.objects.all()

        if buscar:
            redes= Direccion.objects.filter(ip=buscar) 
        return redes.order_by('id')

    def get_initial(self):
        esta_vigente=True
        return {
            
            "esta_vigente":esta_vigente
            
        }





#:_________________________________________________________________________________   SECRETARIA  ___________________________________________________________

#...........................................:::.....Secretaria......::::..............................................

class SecretariavList(ListView):
    model =Secretaria
    template_name ='principal/secretariav/secretariav_list.html'
  
    
    def get_queryset(self):
        return Secretaria.objects.order_by('id')

class SecretariavCreate(CreateView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_formulario.html'
    success_url=reverse_lazy('secretariav_crear')

    queryset=Secretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariavCreateDos(CreateView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Secretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariavUpdate(UpdateView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_edit.html'
    success_url=reverse_lazy('secretariav_listar')

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
       
        context['object']=self.get_object()
        return context
    
class SecretariavDelete(DeleteView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_delete.html'
    success_url=reverse_lazy('secretariav_listar')


class SecretariavDetail(DetailView):
    model =Secretaria
    template_name='principal/secretariav/secretariav_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(SecretariavDetail, self).get_context_data(**kwargs)
        context['unidades_list'] = Unidad.objects.filter(secretaria=self.object)
        context['areas_list'] = Area.objects.filter(secretaria=self.object)
        context['perfil_list'] = Perfil.objects.filter(secretaria=self.object)
        return context

   



#:_________________________________________________________________________________   UNIDAD  ___________________________________________________________

#...........................................:::.....unidad verdadero......::::..............................................

class UnidadvList(ListView):
    model =Unidad
    template_name ='principal/unidadv/unidadv_list.html'

    
    def get_queryset(self):
        return Unidad.objects.order_by('id')

class UnidadvCreate(CreateView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_formulario.html'
    queryset=Unidad.objects.all()

    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })

    def get_initial(self):
        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
        return {
            "secretaria":secretaria,
        }


    def get_context_data(self,**kwargs):
        context=super(UnidadvCreate,self).get_context_data(**kwargs)
        context['object']=Secretaria.objects.get(id=self.kwargs.get("pk"))


        return context

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class UnidadvCreateDos(CreateView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Unidad.objects.all()
    


    def get_initial(self):

        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "secretaria":secretaria,
         
            
        }




    

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class UnidadvUpdate(UpdateView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_edit.html'
    success_url=reverse_lazy('secretariav_listar')

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['object']=self.get_object()
        return context
    
class UnidadvDelete(DeleteView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_delete.html'

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['object']=self.get_object()
        return context

class UnidadvDeletedos(DeleteView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_deletedos.html'
    success_url=reverse_lazy('secretariav_listar')

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })


class UnidadvDetail(DetailView):
    model =Unidad
    template_name='principal/unidadv/unidadv_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(UnidadvDetail, self).get_context_data(**kwargs)
        context['areas_list'] = Area.objects.filter(unidad=self.object)
        context['perfil_list'] = Perfil.objects.filter(unidad=self.object)
        return context

class UnidadvDetailView(DetailView):
    model =Unidad
    template_name='principal/unidadv/unidadv_detail_list_view.html'

    def get_context_data(self, **kwargs):
        context = super(UnidadvDetailView, self).get_context_data(**kwargs)
        context['areas_list'] = Area.objects.filter(unidad=self.object)
        return context


class UnidadvFuncioView(DetailView):
    model =Unidad
    template_name='principal/unidadv/unidadv_funcionarios.html'

    def get_context_data(self, **kwargs):
        context = super(UnidadvFuncioView, self).get_context_data(**kwargs)
        context['areas_list'] = Perfil.objects.filter(unidad=self.object)
        return context


class UnidadAreavCreate(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/unidadv/unidadareav_formulario.html'


    queryset=Area.objects.all()

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('unidadv_detail_list', kwargs={
            'pk':self.object.unidad.id
        })

    def get_initial(self):

        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "unidad":unidad,
        }

    
    
    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
       
        context['object']=Unidad.objects.get(id=self.kwargs.get("pk"))


        return context


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)




#:_________________________________________________________________________________   AREA  ___________________________________________________________

#...........................................:::.....area verdadero......::::..............................................

class AreavList(ListView):
    model =Area
    template_name ='principal/areav/areav_list.html'
 
    
    def get_queryset(self):
        return Area.objects.order_by('id')

class AreavCreate(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formulario.html'
    

    queryset=Area.objects.all()

    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })


    def get_initial(self):

        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "secretaria":secretaria,
         
            
        }
    
    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
       
        context['object']=self.get_object()


        return context


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class AreavCreateDos(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Area.objects.all()
 


    def get_initial(self):

        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "secretaria":secretaria,
         
            
        }

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)




class AreavUniCreate(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formularioUni.html'
    success_url=reverse_lazy('areav_listar')

    queryset=Area.objects.all()
    def get_success_url(self):
        return reverse_lazy('areav_detail_list', kwargs={
            'pk':self.object.id
        })

    def get_initial(self):

        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "unidad":unidad,
         
            
        }


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class AreavUniCreateDos(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formularioUnidos.html'
    success_url=reverse_lazy('areav_listar')

    queryset=Area.objects.all()
    def get_success_url(self):
        return reverse_lazy('areav_detail_list', kwargs={
            'pk':self.object.id
        })


    def get_initial(self):

        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "unidad":unidad,
         
            
        }

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)



    
class AreavUpdate(UpdateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_edit.html'
    success_url=reverse_lazy('secretariav_listar')

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
       
        context['object']=self.get_object()


        return context

class AreavUniUpdate(UpdateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areavUni_edit.html'
   

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('unidadv_detail_list', kwargs={
            'pk':self.object.unidad.id
        })

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['object']=self.get_object()
        return context
    
class AreavDelete(DeleteView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_delete.html'
    success_url=reverse_lazy('secretariav_listar')
    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })

class AreavDeleteT(DeleteView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_deleteT.html'

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('unidadv_detail_list', kwargs={
            'pk':self.object.unidad.id
        })






class AreavDetail(DetailView):
    model =Area
    template_name='principal/areav/areav_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(AreavDetail, self).get_context_data(**kwargs)
        context['areas_list'] = Area.objects.filter(unidad=self.object)
        return context

class AreavSecreDetail(DetailView):
    model =Area
    template_name='principal/areav/areavSecre_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(AreavSecreDetail, self).get_context_data(**kwargs)
        context['areas_list'] = Perfil.objects.filter(area=self.object)
        context['perfil_list'] = Perfil.objects.filter(area=self.object)
        return context

class AreavUnidDetail(DetailView):
    model =Area
    template_name='principal/areav/areavUnid_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(AreavUnidDetail, self).get_context_data(**kwargs)
        context['areas_list'] = Perfil.objects.filter(area=self.object)
        context['perfil_list'] = Perfil.objects.filter(area=self.object)
        return context



class AreavFuncioView(DetailView):
    model =Area
    template_name='principal/areav/areav_funcionarios.html'

    def get_context_data(self, **kwargs):
        context = super(AreavFuncioView, self).get_context_data(**kwargs)
        context['areas_list'] = Perfil.objects.filter(area=self.object)
        return context




#:_________________________________________________________________________________   RELACION RED SECRETARIA  ___________________________________________________________

#...........................................:::.....RED - Secretaria......::::..............................................

class RedSecretariaList(ListView):
    model =RedSecretaria
    template_name ='principal/redSecretaria/redSecretaria_list.html'
    
    
    def get_queryset(self):
        return RedSecretaria.objects.order_by('id')

    def get_initial(self):
        esta_vigente=True
        return {
            
            "esta_vigente":esta_vigente
            
        }

class RedSecretariaCreate(CreateView):
    model =RedSecretaria
    form_class=RedSecretariaFormulario
    template_name='principal/redSecretaria/redSecretaria_formulario.html'
   
    success_url=reverse_lazy('secretariav_listar')
    

    def get_context_data(self,**kwargs):
        context=super(RedSecretariaCreate,self).get_context_data(**kwargs)
        context['object']=Secretaria.objects.get(id=self.kwargs.get("pk"))
        return context


    def get_initial(self):
        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "secretaria":secretaria,
            "esta_vigente":esta_vigente
        }

    queryset=RedSecretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_listar')

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

    

class RedSecretariaCreateDos(CreateView):
    model =RedSecretaria
    form_class=RedSecretariaFormulario
    template_name='principal/redSecretaria/redSecretaria_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=RedSecretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class RedSecretariaUpdate(UpdateView):
    model =RedSecretaria
    form_class=RedSecretariaFormulario
    template_name='principal/redSecretaria/redSecretaria_edit.html'
    success_url=reverse_lazy('secretariav_listar')


    
class RedSecretariaDelete(DeleteView):
    model =RedSecretaria
    form_class=RedSecretariaFormulario
    template_name='principal/redSecretaria/redSecretaria_delete.html'
    success_url=reverse_lazy('secretariav_listar')





class RedSecretariaDetail(DetailView):
    model =RedSecretaria
    template_name='principal/redSecretaria/redSecretaria_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(SecretariavDetail, self).get_context_data(**kwargs)
        context['unidades_list'] = Unidad.objects.filter(secretaria=self.object)
        context['areas_list'] = Area.objects.filter(secretaria=self.object)
        return context




#:_________________________________________________________________________________   RELACION RED Unidad  ___________________________________________________________

#...........................................:::.....RED - Secretaria......::::..............................................

class RedUnidadList(ListView):
    model =RedUnidad
    template_name ='principal/redUnidad/redUnidad_list.html'
    
    
    def get_queryset(self):
        return RedUnidad.objects.order_by('id')

    def get_initial(self):
        esta_vigente=True
        return {
            
            "esta_vigente":esta_vigente
            
        }

class RedUnidadCreate(CreateView):
    model =RedUnidad
    form_class=RedUnidadFormulario
    template_name='principal/redUnidad/redUnidad_formulario.html'
   
    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.unidad.secretaria.id
        })

    def get_context_data(self,**kwargs):
        context=super(RedUnidadCreate,self).get_context_data(**kwargs)
        context['object']=Unidad.objects.get(id=self.kwargs.get("pk"))
        return context



    def get_initial(self):
        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "unidad":unidad,
            "esta_vigente":esta_vigente
        }

    queryset=RedUnidad.objects.all()


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class RedUnidadCreateDos(CreateView):
    model =RedUnidad
    form_class=RedUnidadFormulario
    template_name='principal/redUnidad/redUnidad_formulariodos.html'
    success_url=reverse_lazy('redUnidad_listar')

    queryset=RedUnidad.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class RedUnidadUpdate(UpdateView):
    model =RedUnidad
    form_class=RedUnidadFormulario
    template_name='principal/redUnidad/redUnidad_formulariodos.html'
    success_url=reverse_lazy('redUnidad_listar')
    
class RedUnidadDelete(DeleteView):
    model =RedUnidad
    form_class=RedUnidadFormulario
    template_name='principal/redUnidad/redUnidad_delete.html'
    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.unidad.secretaria.id
        })


class RedUnidadDetail(DetailView):
    model =RedUnidad
    template_name='principal/redUnidad/redUnidad_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(SecretariavDetail, self).get_context_data(**kwargs)
        context['unidades_list'] = Unidad.objects.filter(secretaria=self.object)
        context['areas_list'] = Area.objects.filter(secretaria=self.object)
        return context




#:_________________________________________________________________________________   RELACION RED AREA  ___________________________________________________________

#...........................................:::.....RED - Secretaria......::::..............................................

class RedAreaList(ListView):
    model =RedUnidad
    template_name ='principal/redUnidad/redUnidad_list.html'
    
    
    def get_queryset(self):
        return RedUnidad.objects.order_by('id')

    def get_initial(self):
        esta_vigente=True
        return {
            
            "esta_vigente":esta_vigente
            
        }

class RedAreaCreate(CreateView):
    model =RedArea
    form_class=RedAreaFormulario
    template_name='principal/redArea/redArea_formulario.html'
   
    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.area.secretaria.id
        })

    def get_context_data(self,**kwargs):
        context=super(RedAreaCreate,self).get_context_data(**kwargs)
        context['object']=Area.objects.get(id=self.kwargs.get("pk"))
        return context

    def get_initial(self):
        area=Area.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "area":area,
            "esta_vigente":esta_vigente
        }

    queryset=RedArea.objects.all()


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)






class RedAreaCreateTres(CreateView):
    model =RedArea
    form_class=RedAreaFormulario
    template_name='principal/redArea/redAreaT_formulario.html'
    queryset=RedArea.objects.all()

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('unidadv_detail_list', kwargs={
            'pk':self.object.area.unidad.id
        })

    def get_context_data(self,**kwargs):
        context=super(RedAreaCreateTres,self).get_context_data(**kwargs)
        context['object']=Area.objects.get(id=self.kwargs.get("pk"))
        return context

    def get_initial(self):
        area=Area.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "area":area,
            "esta_vigente":esta_vigente
        }

    
    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class RedAreaCreateDos(CreateView):
    model =RedUnidad
    form_class=RedAreaFormulario
    template_name='principal/redArea/redArea_formulariodos.html'
    success_url=reverse_lazy('redUnidad_listar')

    queryset=RedArea.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class RedAreaUpdate(UpdateView):
    model =RedArea
    form_class=RedAreaFormulario
    template_name='principal/redArea/redArea_formulariodos.html'
    success_url=reverse_lazy('redUnidad_listar')
    
class RedAreaDelete(DeleteView):
    model =RedArea
    form_class=RedAreaFormulario
    template_name='principal/redArea/redArea_delete.html'
    success_url=reverse_lazy('redUnidad_listar')

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.area.secretaria.id
        })


class RedAreaDetail(DetailView):
    model =RedArea
    template_name='principal/redArea/redArea_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(SecretariavDetail, self).get_context_data(**kwargs)
        context['unidades_list'] = Unidad.objects.filter(secretaria=self.object)
        context['areas_list'] = Area.objects.filter(secretaria=self.object)
        return context






#:_________________________________________________________________________________   RELACION PERFIL Secretaria ___________________________________________________________

#...........................................:::.....RED - Secretaria......::::..............................................

class PefilSecretariaList(ListView):
    model =PefilSecretaria
    template_name ='principal/pefilSecretaria/pefilSecretaria_list.html'

    
    def get_queryset(self):
        return PefilSecretaria.objects.order_by('id')

    def get_initial(self):
        esta_vigente=True
        return {
            
            "esta_vigente":esta_vigente
            
        }

class PefilSecretariaCreate(CreateView):
    model =PefilSecretaria
    form_class=PefilSecretariaFormulario
    template_name='principal/pefilSecretaria/pefilSecretaria_formulario.html'
   
    success_url=reverse_lazy('redArea_listar')


    def get_initial(self):
        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "secretaria":secretaria,
            "esta_vigente":esta_vigente
        }

    queryset=RedArea.objects.all()


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class PefilSecretariaCreateDos(CreateView):
    model =PefilSecretaria
    form_class=PefilSecretariaFormulario
    template_name='principal/pefilSecretaria/pefilSecretaria_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=PefilSecretaria.objects.all()
  

    def get_initial(self):
        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "secretaria":secretaria,
            "esta_vigente":esta_vigente
        }

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
       
        context['object']=self.get_object()


        return context



    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)



    
class PefilSecretariaUpdate(UpdateView):
    model =PefilSecretaria
    form_class=PefilSecretariaFormulario
    template_name='principal/pefilSecretaria/pefilSecretaria_formulariodos.html'
    success_url=reverse_lazy('redUnidad_listar')
    
class PefilSecretariaDelete(DeleteView):
    model =PefilSecretaria
    form_class=PefilSecretariaFormulario
    template_name='principal/pefilSecretaria/pefilSecretaria_delete.html'
    success_url=reverse_lazy('redUnidad_listar')


class PefilSecretariaDetail(DetailView):
    model =PefilSecretaria
    template_name='principal/pefilSecretaria/pefilSecretaria_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(SecretariavDetail, self).get_context_data(**kwargs)
        context['unidades_list'] = Unidad.objects.filter(secretaria=self.object)
        context['areas_list'] = Area.objects.filter(secretaria=self.object)
        return context














#:_________________________________________________________________________________  PERFIL CREAR AGREGAR EQUIPO Y IP___________________________________________________________

#...........................................:::.....RED - Secretaria......::::..............................................

class PefilEIList(ListView):
    model =Perfil
    template_name ='principal/pefilEI/pefilEI_list.html'
 
    
    def get_queryset(self):
        return Perfil.objects.order_by('id')

    def get_initial(self):
        esta_vigente=True
        return {
            
            "esta_vigente":esta_vigente
            
        }

class PefilEICreate(CreateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    second_form_class=EquipoFormulario
    third_form_class=RelacionFormulario

    template_name='principal/perfilEI/perfilEI_formulario.html'
   
    success_url=reverse_lazy('perfil_listar')
    
    def get_initial(self):
        perfil=Perfil.objects.get(id=self.kwargs.get("pk"))
        TRUE="true"
        esta_vigente=TRUE
        return {
            "perfil":perfil,
            "esta_vigente":esta_vigente
         
        }

    def get_context_data(self,**kwargs):
        context=super(PefilEICreate,self).get_context_data(**kwargs)
        context['object']=Perfil.objects.get(id=self.kwargs.get("pk"))
        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)

        if 'form3' not in context:
            context['form3']=self.third_form_class(self.request.GET)

  

        return context

    def post(self,request,*args,**kwargs):
        self.object=self.get_object
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)
        form3=self.third_form_class(request.POST)
     
        if form.is_valid() and form2.is_valid()  and form3.is_valid():
            TRUE="True"
            solicitud =form.save(commit=False) 
            solicitud.esta_vigente=TRUE

            equipo=form2.save()
            solicitud.equipos=equipo
           
            
            #solicitud.equipos.relacion1=form3.save()
            relacion=form3.save(commit=False)
            
            relacion.esta_vigente=TRUE
            relacion.equipo=equipo
            relacion.save()

            solicitud.save()

            print(form)
            print("_________________")
            print(form2)
            print("_________________")
            print(form3)
            print("_________________")
            return HttpResponseRedirect(
                reverse_lazy('perfil_detail_list', kwargs={
                    'pk':solicitud.perfil.id
                })
            )
        else:
            return self.render_to_response(self.get_context_data(form=form,form2=form2,form3=form3))


class PefilEICreateDos(CreateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfilEI/perfilEI_formulariodos.html'
    success_url=reverse_lazy('redUnidad_listar')

    queryset=Perfil.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
       
        context['object']=self.get_object()


        return context

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)


class RPefilEICreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    second_form_class=EquipoFormulario
    third_form_class=RelacionPerfilFormulario
    

    template_name='principal/perfilEI/RperfilE_formulario.html'
   
    success_url=reverse_lazy('perfil_listar')

    def get_initial(self):
        direccion=Direccion.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            
            "direccion":direccion,
            "esta_vigente":esta_vigente
        }

    def get_context_data(self,**kwargs):
        context=super(RPefilEICreate,self).get_context_data(**kwargs)
        context['object']=Direccion.objects.get(id=self.kwargs.get("pk"))
        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)

        if 'form3' not in context:
            context['form3']=self.third_form_class(self.request.GET)

        return context

    def post(self,request,*args,**kwargs):
        self.object=self.get_object
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)
        form3=self.third_form_class(request.POST)
       
     
        if form.is_valid() and form2.is_valid()  and form3.is_valid()  :
            TRUE="True"
            
            solicitud =form.save(commit=False) 
            equipo=form2.save()
            solicitud.equipo=equipo
            solicitud.esta_vigente=TRUE

            #solicitud.equipos.relacion1=form3.save()

            

            relacionPerfil=form3.save(commit=False)
            relacionPerfil.esta_vigente=TRUE
            relacionPerfil.equipos=equipo

            relacionPerfil.save()

            solicitud.save()

            print(form)
            print("_________________")
            print(form2)
            print("_________________")
            print(form3)
            print("_________________")
    
            return HttpResponseRedirect(
                reverse_lazy('reddetail_listar', kwargs={
                    'pk':solicitud.direccion.red.id
                })
            )

        else:
            #return self.render_to_response(self.get_context_data(form=form,form2=form2,form3=form3,form4=form4))
            return render(request,self.template_name,self.get_context_data(form=form,form2=form2,form3=form3))





    
class PefilEIUpdate(UpdateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfilEI/perfilEI_formulariodos.html'
    success_url=reverse_lazy('redUnidad_listar')
    
class PefilEIDelete(DeleteView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/pefilEI/perfilEI_delete.html'
    success_url=reverse_lazy('redUnidad_listar')


class PefilEIDetail(DetailView):
    model =Perfil
    form_class=EquipoFormulario
    template_name='principal/perfilEI/perfilEI_detail_list.html'

    

#_________________________________________________AGREGAR EQUIPO - IP _________________________________________-
# _
class EquipoIpCreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    second_form_class=EquipoFormulario
    
    

    template_name='principal/perfilEI/EquipoIp_formulario.html'
   
    success_url=reverse_lazy('perfil_listar')

    def get_initial(self):
        direccion=Direccion.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            
            "direccion":direccion,
            "esta_vigente":esta_vigente
        }

    def get_context_data(self,**kwargs):
        context=super(EquipoIpCreate,self).get_context_data(**kwargs)
        context['object']=Direccion.objects.get(id=self.kwargs.get("pk"))
        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)

        return context

    def post(self,request,*args,**kwargs):
        self.object=self.get_object
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)

     
        if form.is_valid() and form2.is_valid() :
            TRUE="True"
            
            solicitud =form.save(commit=False) 
            equipo=form2.save()
            solicitud.equipo=equipo
            solicitud.esta_vigente=TRUE

            #solicitud.equipos.relacion1=form3.save()


            solicitud.save()

            print(form)
            print("_________________")
            print(form2)
            print("_________________")

    
            return HttpResponseRedirect(
                reverse_lazy('reddetail_listar', kwargs={
                    'pk':solicitud.direccion.red.id
                })
            )

        else:
            #return self.render_to_response(self.get_context_data(form=form,form2=form2,form3=form3,form4=form4))
            return render(request,self.template_name,self.get_context_data(form=form,form2=form2))



#______________________________________________________________________-perfil equipo
class PefilEquipoCreate(CreateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    second_form_class=PerfilFormulario
 

    template_name='principal/perfilEI/PefilEquipo_formulario.html'
   
    

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('reddetail_listar', kwargs={
            'pk':self.object.equipos.relacion1.direccion.id
        })

    def get_initial(self):
        equipos=Equipo.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            
            "equipos":equipos,
            "esta_vigente":esta_vigente
        }
    

    def get_context_data(self,**kwargs):
        context=super(PefilEquipoCreate,self).get_context_data(**kwargs)
        context['object']=Equipo.objects.get(id=self.kwargs.get("pk"))
        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)


        return context

    def post(self,request,*args,**kwargs):
        self.object=self.get_object
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)

     
        if form.is_valid() and form2.is_valid()  :
            TRUE="True"
            
            solicitud =form.save(commit=False) 
            perfil=form2.save()
            solicitud.perfil=perfil
            solicitud.esta_vigente=TRUE

            #solicitud.equipos.relacion1=form3.save()

        
            solicitud.save()

            print(form)
            print("_________________")
            print(form2)
            print("_________________")
     
            return HttpResponseRedirect(
                reverse_lazy('reddetail_listar', kwargs={
                    'pk':solicitud.equipos.relacion.direccion.red.id
                })
            )
        else:
            #return self.render_to_response(self.get_context_data(form=form,form2=form2,form3=form3,form4=form4))
            return render(request,self.template_name,self.get_context_data(form=form,form2=form2))



#:_________________________________________________________________________________  SECRETARIA PERFIL CREAR___________________________________________________________

#...........................................:::.....perfil - Secretaria......::::..............................................


class PefilSecreCreate(CreateView):
    model =PefilSecretaria
    form_class=PefilSecretariaFormulario
    second_form_class=PerfilFormulario
    template_name='principal/secretariav/secretariaPerfil_formulario.html'
  

    def get_initial(self):
        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "secretaria":secretaria,
          
            "esta_vigente":esta_vigente
        }

    def get_success_url(self,**kwargs):
        return reverse_lazy('secretariav_detail_list', kwargs={  
            "pk":self.get_object().secretaria.id
        })

    def get_context_data(self,**kwargs):
        context=super(PefilSecreCreate,self).get_context_data(**kwargs)
        context['secretaria']=Secretaria.objects.get(id=self.kwargs.get("pk"))
        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)


        return context

    def post(self,request,*args,**kwargs):
         #secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
     
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)
    
        if form.is_valid() and form2.is_valid()  :
            solicitud =form.save(commit=False)
            solicitud.perfil=form2.save()
    
            solicitud.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form,form2=form2))



class PefilSecreExisCreate(CreateView):
    model =PefilSecretaria
    form_class=PefilSecretariaFormulario
   
    template_name='principal/secretariaPerfil/secretariaPerfilExis_formulario.html'
  

    def get_initial(self):
        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "secretaria":secretaria,
          
            "esta_vigente":esta_vigente
        }

    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.secretaria.id
        })

    def get_context_data(self,**kwargs):
        context=super(PefilSecreExisCreate,self).get_context_data(**kwargs)
        context['object']=Secretaria.objects.get(id=self.kwargs.get("pk"))
        return context

    





#:_________________________________________________________________________________  UNIDAD PERFIL CREAR___________________________________________________________

#...........................................:::.....perfil - Secretaria......::::..............................................



class PefilUnidadCreate(CreateView):#relacion perfil unidad 
    model =PefilUnidad
    form_class=PefilUnidadFormulario
    template_name='principal/pefilUnidad/pefilUnidad_formulario.html'
    success_url=reverse_lazy('relacion_listar')

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('unidadv_detail_list', kwargs={
            'pk':self.object.unidad.id
        })

    def get_initial(self):
        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "unidad":unidad,
            "esta_vigente":esta_vigente
        }

    def get_context_data(self,**kwargs):
        context=super(PefilUnidadCreate,self).get_context_data(**kwargs)
        context['object']=Unidad.objects.get(id=self.kwargs.get("pk"))
        return context

        
    



class UniPerfilCreate(CreateView):
    model =PefilUnidad
    form_class=PefilUnidadFormulario
    second_form_class=PerfilFormulario
    template_name='principal/unidadv/unidadPerfil_formulario.html'


    def get_initial(self):
        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "unidad":unidad,
            "esta_vigente":esta_vigente
        }

    def get_success_url(self,**kwargs):
        return reverse_lazy('secretariav_detail_list', kwargs={  
            "pk":self.get_object().secretaria.id
        })


    def get_context_data(self,**kwargs):
        context=super(UniPerfilCreate,self).get_context_data(**kwargs)
        context['unidad']=Unidad.objects.get(id=self.kwargs.get("pk"))
        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)


        return context

    def post(self,request,*args,**kwargs):
         #secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
     
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)
    
        if form.is_valid() and form2.is_valid()  :
            solicitud =form.save(commit=False)
            solicitud.perfil=form2.save()
    
            solicitud.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form,form2=form2))

  
 

  #:_________________________________________________________________________________  AREA PERFIL CREAR___________________________________________________________

#...........................................:::.....perfil - Secretaria......::::..............................................
class AreaPerfilCreate(CreateView):#relacion perfil unidad 
    model =PefilArea
    form_class=PefilAreaFormulario
    template_name='principal/areaPerfil/areaPerfil_formulario.html'
    success_url=reverse_lazy('secretariav_listar')

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('areavUnid_detail_list', kwargs={
            'pk':self.object.area.id
        })

    def get_initial(self):
        area=Area.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "area":area,
            "esta_vigente":esta_vigente
        }
    
    def get_context_data(self,**kwargs):
        context=super(AreaPerfilCreate,self).get_context_data(**kwargs)
        context['object']=Area.objects.get(id=self.kwargs.get("pk"))
        return context



    


class AreaEICreate(CreateView):
    model =PefilArea
    form_class=PefilAreaFormulario
    second_form_class=PerfilFormulario
    template_name='principal/areav/areaPerfil_formulario.html'
    success_url=reverse_lazy('secretariav_listar')

    def get_initial(self):
        area=Area.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "area":area,
            "esta_vigente":esta_vigente
        }

# ERROR EN ESTA PARTE
    def get_context_data(self, **kwargs):
        context=super(AreaEICreate,self).get_context_data(**kwargs)
        context['object']=self.get_object()

        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)

        return context

    def post(self,request,*args,**kwargs):
        self.object=self.get_object
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)
    
        if form.is_valid() and form2.is_valid()  :
            solicitud =form.save(commit=False)
            solicitud.perfil=form2.save()
    
            solicitud.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form,form2=form2))





#:_________________________________________________________________________________  PERFIL CREAR AGREGAR EQUIPO Y IP___________________________________________________________

#...........................................:::.....RED - Secretaria......::::..............................................



class IPPefilECreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    second_form_class=EquipoFormulario
    third_form_class=RelacionPerfilFormulario
    forth_form_class=PerfilFormulario

    template_name='principal/perfilEI/IperfilE_formulario.html'
   
    

    def get_success_url(self):
        print("_________________")
        print(self.object)
        print("_________________")
        return reverse_lazy('reddetail_listar', kwargs={
            'pk':self.object.direccion.red.id
        })

    def get_initial(self):
        direccion=Direccion.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            
            "direccion":direccion,
            "esta_vigente":esta_vigente
        }
    

    def get_context_data(self,**kwargs):
        context=super(IPPefilECreate,self).get_context_data(**kwargs)
        context['object']=Direccion.objects.get(id=self.kwargs.get("pk"))
        if 'form' not in context:
            context['form']=self.form_class(self.request.GET)

        if 'form2' not in context:
            context['form2']=self.second_form_class(self.request.GET)

        if 'form3' not in context:
            context['form3']=self.third_form_class(self.request.GET)

        if 'form4' not in context:
            context['form4']=self.forth_form_class(self.request.GET)

        return context

    def post(self,request,*args,**kwargs):
        self.object=self.get_object
        form = self.form_class(request.POST)
        form2=self.second_form_class(request.POST)
        form3=self.third_form_class(request.POST)
        form4=self.forth_form_class(request.POST)
     
        if form.is_valid() and form2.is_valid()  and form3.is_valid() and form4.is_valid() :
            TRUE="True"
            
            solicitud =form.save(commit=False) 
            equipo=form2.save()
            solicitud.equipo=equipo
            solicitud.esta_vigente=TRUE

            #solicitud.equipos.relacion1=form3.save()

            perfil=form4.save()
            

            relacionPerfil=form3.save(commit=False)
            relacionPerfil.esta_vigente=TRUE
            relacionPerfil.equipos=equipo
            relacionPerfil.perfil=perfil

            relacionPerfil.save()

            solicitud.save()

            print(form)
            print("_________________")
            print(form2)
            print("_________________")
            print(form3)
            print("_________________")
            print(form4)
            print("_________________")
            return HttpResponseRedirect(
                reverse_lazy('reddetail_listar', kwargs={
                    'pk':solicitud.direccion.red.id
                })
            )
        else:
            #return self.render_to_response(self.get_context_data(form=form,form2=form2,form3=form3,form4=form4))
            return render(request,self.template_name,self.get_context_data(form=form,form2=form2,form3=form3,form4=form4))
                










































































































#...............................................................:::.....Infraestructura......::::...........................................................

class InfraestructuraList(ListView):
    model =Infraestructura
    template_name ='principal/infraestructura/infraestructura_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return Infraestructura.objects.order_by('id')

class InfraestructuraCreate(CreateView):
    model =Infraestructura
    form_class=InfraestructuraFormulario
    template_name='principal/infraestructura/infraestructura_formulario.html'
    success_url=reverse_lazy('infraestructura_listar')
    queryset=Perfil.objects.all()
    def get_success_url(self):
        return reverse_lazy('infraestructura_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
        

    
class InfraestructuraUpdate(UpdateView):
    model =Infraestructura
    form_class=InfraestructuraFormulario
    template_name='principal/infraestructura/infraestructura_formulario.html'
    success_url=reverse_lazy('infraestructura_listar')
    
class InfraestructuraDelete(DeleteView):
    model =Infraestructura
    form_class=InfraestructuraFormulario
    template_name='principal/infraestructura/infraestructura_delete.html'
    success_url=reverse_lazy('infraestructura_listar')


class InfraestructuraDetail(DetailView):
    model =Infraestructura
    template_name='principal/infraestructura/infraestructura_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['infraestructura_list']=self.get_object().infraestructurasecretaria_set.all()
        
        context['infraestructuraunidad_list']=self.get_object().infraestructuraunidad_set.all() 
        return context


     


#...........................................:::.....Secretariauno......::::..............................................

class SecretariaList(ListView):
    model =Secretariauno
    template_name ='principal/secretaria/secretaria_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return Secretariauno.objects.order_by('id')

class SecretariaCreate(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariaCreateDos(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulariodos.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariaUpdate(UpdateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')
    
class SecretariaDelete(DeleteView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_delete.html'
    success_url=reverse_lazy('secretaria_listar')


class SecretariaDetail(DetailView):
    model =Secretariauno
    template_name='principal/secretaria/secretaria_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['secretaria_list']=self.get_object().secretariaarea_set.all() 
        return context


#...................................................................Secretaria Area.................................................
class SecretariaAreaList(ListView):
    model =SecretariaArea
    template_name ='principal/secretariaArea/secretariaArea_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return SecretariaArea.objects.order_by('id')

class SecretariaAreaCreate(CreateView):
    model =SecretariaArea
    form_class=SecretariaAreaFormulario
    template_name='principal/secretariaArea/secretariaArea_formulario.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariaAreaCreateDos(CreateView):
    model =SecretariaArea
    form_class=SecretariaAreaFormulario
    template_name='principal/secretariaArea/secretariaArea_formulariodos.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariaArea_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariaAreaUpdate(UpdateView):
    model =SecretariaArea
    form_class=SecretariaFormulario
    template_name='principal/secretariaArea/secretariaArea_formulario.html'
    success_url=reverse_lazy('secretaria_listar')
    
class SecretariaAreaDelete(DeleteView):
    model =SecretariaArea
    form_class=SecretariaAreaFormulario
    template_name='principal/secretariaArea/secretariaArea_delete.html'
    success_url=reverse_lazy('secretaria_listar')


class SecretariaAreaDetail(DetailView):
    model =SecretariaArea
    template_name='principal/secretariaArea/secretariaArea_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['secretaria_list']=self.get_object().area_set.all() 
        return context


#...................................................................Secretaria unidad.................................................
class SecretariaUnidadList(ListView):
    model =Secretariauno
    template_name ='principal/secretaria/secretaria_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return Secretariauno.objects.order_by('id')

class SecretariaUnidadCreate(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariaUnidadCreateDos(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulariodos.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariaUnidadUpdate(UpdateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')
    
class SecretariaUnidadDelete(DeleteView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_delete.html'
    success_url=reverse_lazy('secretaria_listar')


class SecretariaUnidadDetail(DetailView):
    model =Secretariauno
    template_name='principal/secretaria/secretaria_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['secretaria_list']=self.get_object().area_set.all() 
        return context






#...........................................:::.....Unidad......::::..............................................

class UnidadList(ListView):
    model =Unidaduno
    template_name ='principal/unidad/unidad_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return Unidaduno.objects.order_by('id')

class UnidadCreate(CreateView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_formulario.html'
    success_url=reverse_lazy('unidad_listar')


class UnidadCreateDos(CreateView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_formulario.html'
    success_url=reverse_lazy('unidad_listar')
    
class UnidadUpdate(UpdateView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_formulario.html'
    success_url=reverse_lazy('unidad_listar')
    
class UnidadDelete(DeleteView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_delete.html'
    success_url=reverse_lazy('unidad_listar')


class UnidadDetail(DetailView):
    model =Unidaduno
    template_name='principal/unidad/unidad_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context


#...........................................:::.....Area......::::..............................................

class AreaList(ListView):
    model =Areauno
    template_name ='principal/area/area_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Areauno.objects.order_by('id')

class AreaCreate(CreateView):
    model =Areauno
    form_class=AreaFormulario
    template_name='principal/area/area_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class AreaUpdate(UpdateView):
    model =Areauno
    form_class=AreaFormulario
    template_name='principal/area/area_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class AreaDelete(DeleteView):
    model =Areauno
    form_class=AreaFormulario
    template_name='principal/area/area_delete.html'
    success_url=reverse_lazy('area_listar')


class AreaDetail(DetailView):
    model =Areauno
    template_name='principal/area/area_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context























































































































#...........................................:::.....Relacion  InfraestructuraSecretaria......::::..............................................

class InfraestructuraSecretariaList(ListView):
    model =InfraestructuraSecretaria
    template_name ='principal/infraestructuraSecretaria/infraestructuraSecretaria_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return InfraestructuraSecretaria.objects.order_by('id')

class InfraestructuraSecretariaCreate(CreateView):
    model =InfraestructuraSecretaria
    form_class=InfraestructuraSecretariaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_formulario.html'
    success_url=reverse_lazy('infraestructuraSecretaria_listar')
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
         
            
        }

class InfraestructuraSecretariaCreatedos(CreateView):
    model =InfraestructuraSecretaria
    form_class=InfraestructuraSecretariaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_formulariodos.html'
    success_url=reverse_lazy('infraestructuraSecretaria_listar')
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
        
        }
    

class InfraestructuraSecretariaUpdate(UpdateView):
    model =InfraestructuraSecretaria
    form_class=AreaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraSecretariaDelete(DeleteView):
    model =InfraestructuraSecretaria
    form_class=AreaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_delete.html'
    success_url=reverse_lazy('area_listar')


class InfraestructuraSecretariaDetail(DetailView):
    model =InfraestructuraSecretaria
    template_name='principal/InfraestructuraSecretaria/infraestructuraSecretaria_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context

#...........................................:::.....Relacion  InfraestructuraUnidad  ......::::..............................................

class InfraestructuraUnidadList(ListView):
    model =InfraestructuraUnidad
    template_name ='principal/infraestructuraUnidad/infraestructuraSecretaria_list.html'
    #paginate_by=20
    
    def get_queryset(self):
        return InfraestructuraUnidad.objects.order_by('id')

class InfraestructuraUnidadCreate(CreateView):
    model =InfraestructuraUnidad
    form_class=InfraestructuraUnidadFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_formulario.html'
    
    success_url=reverse_lazy('infraestructuraUnidad_listar')
    
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
         
            
        }

class InfraestructuraUnidadCreateDos(CreateView):
    model =InfraestructuraUnidad
    form_class=InfraestructuraUnidadFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_formulariodos.html'
    

    success_url=reverse_lazy('infraestructuraUnidad_listar')
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
         
            
        }


    
class InfraestructuraUnidadUpdate(UpdateView):
    model =InfraestructuraUnidad
    form_class=AreaFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraUnidadDelete(DeleteView):
    model =InfraestructuraUnidad
    form_class=AreaFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_delete.html'
    success_url=reverse_lazy('area_listar')


class InfraestructuraUnidadDetail(DetailView):
    model =InfraestructuraUnidad
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context


#...........................................:::.....Relacion  InfraestructuraArea ......::::..............................................

class InfraestructuraAreaList(ListView):
    model =InfraestructuraArea
    template_name ='principal/infraestructuraArea/infraestructuraArea_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return InfraestructuraArea.objects.order_by('id')

class InfraestructuraAreaCreate(CreateView):
    model =InfraestructuraArea
    form_class=AreaFormulario
    template_name='principal/infraestructuraArea/infraestructuraArea_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraAreaUpdate(UpdateView):
    model =InfraestructuraArea
    form_class=AreaFormulario
    template_name='principal/infraestructuraArea/infraestructuraArea_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraAreaDelete(DeleteView):
    model =InfraestructuraArea
    form_class=AreaFormulario
    template_name='principal/infraestructuraArea/infraestructuraArea_delete.html'
    success_url=reverse_lazy('area_listar')


class InfraestructuraUnidadDetail(DetailView):
    model =InfraestructuraArea
    template_name='principal/infraestructuraArea/infraestructuraArea_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context