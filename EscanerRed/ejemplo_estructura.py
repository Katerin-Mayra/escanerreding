class GeneralModel(models.Model):
    esta_activo = models.BooleanField(default = True)
    creado_en = models.DateTimeField(auto_now_add = True, null = True)
    modificado_en = models.DateTimeField(auto_now = True, null = True)
    orden = models.IntegerField(default = 0)
    
    class Meta:
        abstract = True

class Secretaria(GeneralModel):
    nombre = models.CharField(max_length = 128)
    sigla = models.CharField(max_length = 16, unique = True)
    tipo = models.CharField(max_length = 16)
    etiqueta = models.CharField(max_length = 128, null = True, blank = True)

    def __str__(self):
        return '{}'.format(self.nombre)

    class Meta:
        verbose_name = 'Secretaría'

class Unidad(GeneralModel):
    nombre = models.CharField(max_length = 128)
    sigla = models.CharField(max_length = 16, unique = True)
    tipo = models.CharField(max_length = 16)
    etiqueta = models.CharField(max_length = 128, null = True, blank = True)
    secretaria = models.ForeignKey(Secretaria, null = True, related_name = 'unidades', on_delete = models.SET_NULL)
    
    def __str__(self):
        return '{}'.format(self.nombre)
    class Meta:
        verbose_name = 'Unidades'

class Area(GeneralModel):
    nombre = models.CharField(max_length = 128)
    sigla = models.CharField(max_length = 16, unique = True)
    tipo = models.CharField(max_length = 16, default = "ÁREA")
    etiqueta = models.CharField(max_length = 128, null = True, blank = True)
    secretaria = models.ForeignKey(Secretaria, null = True, blank = True, related_name = 'areas', on_delete = models.SET_NULL)
    unidad = models.ForeignKey(Unidad, null = True, blank = True, related_name = 'areas', on_delete = models.SET_NULL)

    def __str__(self):
        return '{}'.format(self.nombre)

    class Meta:
        verbose_name = 'Área'
